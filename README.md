# rudy


A cool way for percussionists and drummers to practice their rudiments.

![0.png](png/0.png)

## INSTRUCTIONS

* Download zip file pressing the download button above.

* Unzip

* Double-click on index.html file

* __Click on the Rudy button__ to change combination of rudiments.

* __Hover__ over any rudiment, and select the __speed__ to play the audio.



**Enjoy!**

[Audio Source](https://www.youtube.com/watch?v=ONTwonwbnNA&t=4sl)
